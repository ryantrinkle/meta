# Haskell Foundation Ways of Working

This "meta" repo describes how the Haskell Foundation Board aspires to
work to meet the goals of the Foundation.

If there is a conflict between this document and the Bylaws, the latter
have precedence.

If you want to change the way the Haskell Foundation does its business,
please make an MR or file an Issue against this repo.

Important documents in this repo and elsewhere about the Haskell Foundation:

* The [Haskell Foundation and its Board](board.md)

* The [State of the Haskell Foundation](https://docs.google.com/spreadsheets/d/1oRoa3uTCVZMWbLbM8Q_L06Os3yLzTAcrXIxFQWI-Z_I/edit?usp=sharing)
  lists the Board members, Executive Team, and our roles.

* How we [communicate](communication.md) among ourselves and to the world.


