# 1. Goals of the Foundation
The Haskell Foundation (HF) is an independent, non-profit organization dedicated
to broadening the adoption of Haskell, by supporting its ecosystem of tools,
libraries, education, and research.

# 2. The values of the Foundation
How we pursue the goals of HF is just as important as what the goals are. The actions
of the Foundation and its Board will be guided by these core principles:

* **Open source**. Haskell is an open source community and HF will embrace the
  open-source ethos wholeheartedly. HF may develop, or sponsor the development
  of tools and infrastructure, but it will all be open source.

* **Empowering the community**. A major goal of HF is to boost, celebrate, and
  coordinate the contributions and leadership of volunteers, not to supplant or
  replace them.

* **Open, friendly, and diverse**. For many of us Haskell is more a way of life
  than a programming language. We strive to make sure that all are welcome, that
  all can contribute, and that it can all be done in a way that empowers our
  community members.

* **Transparent**. All formal decision making will be publicly accessible. We
  also strive to share ideas early. This is to enable asynchronous communication
  and collaboration. Only certain categories of sensitive information (e.g.
  financial, and matters concerning particular individuals) will be kept confidential.

* **True to Haskell’s principles**. Haskell's design puts principle ahead of what is
  easy or popular by being firmly guided by the principles of purely functional programming.
  Success, yes, but not at all costs!

# 3. What the Board does
The Board provides the strategic leadership for the Foundation, and is the
decision-making body for everything the Foundation does. More specifically:

* Governance: establishing operating policies, leadership, direction, setting strategy,
  providing guidance
* Staff: appoint and manage senior members of Foundation staff
* Define, curate and track Foundation goals
* Deploy the funds raised by the Foundation to support the Foundation’s goals
* Seek out opportunities to further the goals of the Foundation
* Represent the Haskell community to the world: liaise with sponsors, public bodies
  (ACM, standards committees), etc.
* Ensure success and long-term continuity of the Foundation
* Receive and review financial accounts

# 4. Expectations of Board Members
This section covers what it means to be a member of the Board, and how members aspire
to behave.

* Despite being unpaid, membership on the Board is not an honorary post; it involves
  real work. Members seek to be proactive, not reactive, in developing an inspiring
  vision for the Foundation, and helping to turn that vision into reality. They make
  it a priority to attend meetings, and respond in a timely way to Board discussions.

* Board members are expected to be willing to serve on or chair committees and/or task
  forces that the Board or Executive Team establish.

* Board members strive to act in the best interests of the Foundation and the entire
  Haskell community; they can and should represent the interests of parts of the community
  that they know and understand, but they listen to the views of others, and seek
  collective decisions that balance all those interests.

* Board members abide by Board decisions in public, even if they personally would have
  made a different decision.  That personal view does not have to be secret, but members
  should make it clear that they accept the consensus decision, and will do nothing to
  undermine it.  (If that is not possible, the proper course is to resign from the Board.)

* As prominent members of the Haskell community, Board members aspire to follow the
  [Haskell Guidelines for Respectful Communication](https://haskell.foundation/guidelines-for-respectful-communication/) in all their online communications
  in the Haskell sphere.

* So far as board meetings are concerned, members recognise that (by design) the board is
  diverse in terms of age and experience, and that all have points of view that should be
  valued and shared. Members strive to express their views persuasively but not
  confrontationally, and to contribute to the conversation without dominating it.

# 5. Specific roles within the Board
The Board appoints the following roles from among its membership:
* Chair and Vice Chair
* Treasurer and Vice Treasurer
* Secretary and Vice Secretary

Whenever and as long as the Chair is unable to fulfil their duties the Vice Chair becomes
the acting Chair. Similarly the Vice Treasurer and Vice Secretary.

The primary roles must be held by different individuals, though the "vice" positions may
overlap with the primary ones.

The Board holds an annual election for all of these roles.  If any of them become unavailable
permanently or for an indeterminate period of time, an ad-hoc election can be held. 

Any member of the Board can nominate themselves or others to be elected to any of these roles.

## 5.1. Chair
The Chair is a servant to the Board and the guardian of its ways of working.  It is the
responsibility of the Chair to execute the operations of the Board. Most notably this includes
scheduling and chairing Board meetings, shepherding the voting process, and carrying out or
delegating action items out of Board resolutions.

## 5.2. Treasurer
The Treasurer is responsible for overseeing the financial activities of the Foundation. This
includes authorizing tax filings, financial instruments, and transactions, reporting to the
Board on the finances of the Foundation, and proposing budgets and financial policies for the
Board to adopt.

## 5.3. Secretary
The key role of the Secretary is to be the official source of the history of decisions of the
Board. That is, if we need to know whether some resolution was actually passed, we ask the
Secretary. To fulfil this duty, the Secretary is also responsible for the creation, storage,
and publication of meeting minutes, though it is expected that other members of the Board
help drafting the minutes.

The Secretary orchestrates the selection process for new members, described below.

The Secretary is furthermore the guardian of the Board's commitment to transparency.
Accordingly, the Secretary publishes minutes in a timely manner and is responsible for making
periodic announcements to the community of Board activities. The Secretary ensures that
internal communication within the Board is performed in a way that supports our transparency
goals -- for example, that discussions about decision-making are done in a way suitable for
public release.

# 6. Board meetings

We aspire to conduct our business, and especially to make our decisions, in a way that is
inclusive, collaborative, and transparent.  We want everyone’s voice to be heard at meetings.

When there are disagreements we seek to resolve them to everyone’s satisfaction. Taking a
formal vote on a topic where there is serious disagreement may occasionally be necessary,
but should be a rarity.

## 6.1. Schedule

The Board meets regularly, with Board meetings called by the Chair.

Meetings are scheduled in a manner as equitable as possible to our varied membership. 
Specifically, meetings will not be scheduled in such a way that any one Member is consistently
prevented from participating.

## 6.2. Before meetings

If there are any documents (e.g. proposals, contracts, etc.) to discuss during the meeting,
the Chair will email these documents to the Board mailing list no less than 48 hours before
the meeting. Board members will read these documents before the meeting begins.

## 6.3. Agendas

A meeting's agenda is available at least 48 hours before the start of the meeting.

Meeting participants may edit the agenda in advance of the meeting to add items or clarifying
questions, but will not remove others' agenda items or otherwise subvert agendas.

The Chair decides on the order of the agenda items, and is generally open for input to this end.

Agenda items that are not being discussed because of time restrictions automatically roll over
to the next meeting's agenda.

After the meeting, the agenda is made public by being added to the [minutes repository](https://gitlab.haskell.org/hf/minutes).

## 6.4. Minutes
Minutes are taken for all board meetings, and subsequently published in the [minutes repository](https://gitlab.haskell.org/hf/minutes) so that the Haskell
community can see what the Board has been up to.

### 6.3.1. Before the meeting

The Secretary creates and distributes the minutes document at the beginning
of the meeting.

### 6.3.2. At the meeting
All meeting participants are expected to collaboratively contribute to the meeting minutes.
The Secretary is ultimately responsible for the accurate capturing of the meeting minutes.

### 6.3.3. After the meeting
Attendees of the minutes edit the minutes within 24 hours of the meeting to correct any misquotes,
etc., that were recorded.  The Secretary is responsible to review the minutes after those 24
hours to ensure they are appropriate for publishing.

### 6.3.4. Ratification and publication
* Minutes are automatically ratified 24 hours after the end of the meeting or as soon as all
  pending objections have been resolved, whichever comes first.

* The Secretary has the authority to redact certain details from the minutes before posting.
  These details might concern, for example, personnel decisions or not-yet-closed fund-raising
  negotiations.

* The Secretary is responsible for publishing the minutes in a timely manner after ratification.

# 7. Board Membership Lifecycle

Any community thrives best by continuing to refresh itself with new members. This section describes the process by which the Board refreshes itself.

## 7.1. Goals

There are several goals of the design of this process:

* A way for any member of the community to apply for a seat on the Board
* Resilience in the face of unexpected departures from the Board
* Avoidance of too-frequent calls for applications
* Accommodation for a scenario when more or fewer excellent candidates apply
  than the number of leaving Board members
* A desire to keep the size of the Board at 8 or above and at no more than 14

While the sections below spell out the rules, we give a brief overview here.
When the Board is about to drop below 8 members, we put out a call for
applications. These applications are then considered (in private) by remaining
Board members, who will then vote on candidates using the procedure described
below. The successful candidates will subsequently be admitted to the Board.

## 7.2. Terms

An appointment to the Board carries a term of 3 years (but see Balancing Terms, below).
To avoid short-term gaps between terms, terms are extended to expire only at the end of
an application process.

Any member of the Board is free to resign from the Board at any time; such a
member may choose to leave the Board immediately or to wait until the end of
an application process.

A member whose term is expiring is free to re-apply, but only for one second term. (That is,
a member may serve a maximum of two consecutive terms.) This restriction lifts
after the member has been away from the Board for six months. 

## 7.3. Triggering an Application Process

We call a member who has served beyond their term (normally, three
years) an "expiring" member.

An application process is triggered on February 1 every year. There
is one exception to this rule: if the number of unexpiring members
is already 14, then we have no more room and will not run an application
process.

The result of the application process is an appointment of zero or more
directors, such that the total number of unexpiring members stays below or
equal to 14.

When an application process is triggered, the Secretary orchestrates the
application process, below.

## 7.4. Application Process

At the start of an application process, but no later than February 10,
the Secretary puts out a public call
for applications for community members to join the Board and collects
responses from the community. Applications are due by March 1.

Individual members of the Board are free to communicate the call for applications
and to recruit members of the community to apply. This may be useful, for example,
in increasing the diversity of representation on the Board. The Board may wish to
form an ad-hoc recruitment committee to brainstorm individuals to invite, though
this document does not dictate the rules of such a committee.

Applications include a CV and a cover letter. The cover letter describes why
the applicant wishes to join the Board and how they feel they could contribute
best. It should also address how the applicant meets the criteria below.

After March 1 but before March 10, the Secretary calls a meeting of
unexpiring Board members to discuss the applications. This meeting must happen
by March 20; no minutes are taken.

When the Secretary's
term is expiring, the Vice Secretary fills in for the Secretary to run this
process. If both are expiring, the Chair nominates an unexpiring member
to run this process.

## 7.5. Voting

[Shulze method]: https://en.wikipedia.org/wiki/Schulze_method

Unexpiring members of the Board vote on new membership via a ranked voting
system, with winners decided using the [Shulze
method]. This method takes as input a ranked list (ties allowed) of choices from each
voting member and produces a
ranking of choices as its output. One of the choices in this vote is
"That's it", where any applicant whose results fall below "That's it" in the
ranking order is not appointed. (If "That's it" ties with one or more
applicants, then those applicants are selected into the Board.) This process
allows some fluctuation in the size of the Board, in order to take advantage
of a crop of good volunteers appearing all at once.

The application and voting process is kept private, by using direct email to
Board members. Board members can choose to cast their vote visibly to the rest
of the (unexpiring) Board, offering explanations; or a Board member can choose
to email the Secretary directly and thus keep their preferences more private.
Because the votes can contain explanations, some Board members may choose to
update their votes. This is allowed up until April 1; only the final vote counts.

If the result of the election would lead to a Board of greater than 14, only
enough applicants are selected to bring the total size of the Board to 14.
(This effectively moves "That's it" up in the rankings.) In the event that the
size-14 cutoff falls in the middle of a tie, the tied applicants are not
appointed. (This keeps the Board at a maximum size of 14.)

For example, suppose Soomin Kim, Chris Young, and Jens Pedersen are applying.
One ranking might be

1. Chris Young
2. Soomin Kim
3. That's it
4. Jens Pedersen

This ranking means that the voter wishes Chris and Soomin to join the board,
but for Jens not to.

This voting system has been used for several years by the GHC Steering
Committee. While somewhat complex to describe, it is easy to understand and
use as a voter and works well in practice.

## 7.6. Selection Criteria

While we do not expect all applicants to meet all criteria, here are the
qualities we seek in an application:

* You have a positive drive and vision for the Haskell community and ecosystem.
* You have a track record of contribution to the Haskell community and ecosystem.
* You are widely trusted and respected in the community.
* You have enough time and energy to devote to being a member of the board; it
   is not an honorary position!
* You are at least 18 years of age.

Furthermore, we wish the Board to be broadly representative of the Haskell
community we would like to foster. We thus aim to compose a Board that,
collectively, satisfies these criteria:

1. Includes individuals with the skills, expertise and experience (e.g.
   technical, legal, organisational, community-building) that the Board needs.

1. Reflects the rich diversity (e.g. of age, gender, geographical spread) that
   is in the Haskell community.

1. Includes individuals who are well-equipped to reflect the priorities of
   Haskell’s various constituencies, including

   * Companies that use Haskell in production, and Haskell consultancies;
     giving this group a stronger voice is one of the HF’s main goals.
  
   * Users of Haskell. That might include companies, but also includes the
     broader open-source community, hobbyists, etc.

   * Sponsors: companies (or even individuals) who are funding the Foundation.
  
   * People who build and run the infrastructure of the Haskell ecosystem:
     compilers, libraries, packaging and distribution, IDEs etc.
  
   * Educators, including school, university, and commercial training courses.
  
   * Functional programming researchers who build on and/or develop Haskell.
  
Simultaneously hitting all these criteria is nigh impossible. However, each
subsequent round of applications for new Board members offers a fresh chance
to rectify any imbalances.

## 7.7. Balancing Terms

Ideally, roughly a third of the Board would expire every year. 
After the initial appointment of Board members and occasionally thereafter
(if there are early resignations, for example), the dates of expiry of Board
members' terms may however become unbalanced. The Board may vote in a new
slate of term lengths (with new term lengths varying from 2 to 4 years)
by a two-thirds majority vote. These term lengths would be retroactive.

It is expected that the Secretary would notice this imbalance and call for
this rebalancing, though any member of the Board is free to raise this issue
and call for such a vote.

For example, suppose we have the following Board members with given start
dates and terms:

| Member | Start date | Term | Expiry   |
|--------|------------|------|----------|
| A      | Jan 2022   | 3y   | Jan 2025 |
| B      | Jan 2022   | 3y   | Jan 2025 |
| C      | Jan 2022   | 3y   | Jan 2025 |
| D      | Jan 2022   | 3y   | Jan 2025 |
| E      | Jan 2023   | 3y   | Jan 2026 |
| F      | Jan 2024   | 3y   | Jan 2027 |

This is unbalanced, as four of the six members all expire in Jan 2025. Note
that all members have a term-length of 3 years, the default. When
the Board notices this imbalance, they would decide on a new slate of term
lengths, such as this one:

| Member | Term |
|--------|------|
| A      | 3y   |
| B      | 2y   |
| C      | 3y   |
| D      | 4y   |
| E      | 3y   |
| F      | 3y   |

This changes the term lengths of B and D. With a two-thirds majority vote,
this new slate could be accepted. The updated table of expirations would become:

| Member | Start date | Term | Expiry   |
|--------|------------|------|----------|
| A      | Jan 2022   | 3y   | Jan 2025 |
| B      | Jan 2022   | 2y   | Jan 2024 |
| C      | Jan 2022   | 3y   | Jan 2025 |
| D      | Jan 2022   | 4y   | Jan 2026 |
| E      | Jan 2023   | 3y   | Jan 2026 |
| F      | Jan 2024   | 3y   | Jan 2027 |

Note how B now expires in 2024 and D now expires in 2026. Since term
lengths are retroactive, we calculate the new dates of expiry by adding
the length of term to the original date of appointment. This might
mean that some members expire immediately, in which case the usual rules
for seeking applications for new members would take effect.



# 8. Committees

Board Committees (or Committees for short) are an organisational tool of the
Board. Their main purpose is to allow for efficient execution and decision
making as well as self-governance of the Board by delegating certain
responsibilities and powers to a subset of the
Board's members. As examples, we might have a Budget Committee whose remit is
to pay attention to budgetary issues or a Recruitment Committee to consider members
of the community we wish to invite to join the Board.

Committees are distinguished from *Task Forces*, which report to the Executive
Director (or their designee). A Task Force is not concerned with Board goveranance
or oversight of the HF, but instead with enacting a plan to improve or support the
Haskell language ecosystem. As examples, we might have a Core Libraries Task Force
or a Community Moderation Task Force. Note that both of these have *external* deliverables
to the Haskell community, while Committees will tend to have *internal* deliverables
to the HF. The key distinction, though, is one of reporting: a Committee reports to the
Board directly, while a Task Force reports to an employee of the HF.

*Purpose.* The Board forms a Committee with a specific purpose in mind, giving
the Committee a befitting name and set of responsibilities. These, and other
details, are written in a charter that describes the functioning of the
Committee.

*Powers.* Committees can act by recommending a course of action to the Board,
but the Board may also delegate specific powers to that Committee. The
Committee acts with only those powers; any other exercise of Board powers
must be approved by the Board using its usual voting practices. Examples
of delegated powers include spending and receiving money and adjusting 
budgets, approving specific contracts, and requiring certain actions
from the executive team.

*Terms.* A Committee may be standing (with an indefinite term) or tactical.
When forming a tactical committee, the charter describes a triggering event
(e.g. a date or a document having been approved) that, when it occurs, marks
the conclusion of the work of the Committee.

*Membership.* At all times, a Committee is headed by a Chair, who must be a
member of the Board. A Committee may additionally include one or more members,
who also must be members of the Board. Finally, a Committee may involve
external subject matter experts (SME) if their expertise or work is required.

A Committee has membership rules that allow for adding, replacing and removing
its members, including the Chair, and its SMEs. For Committees without
delegated powers, the default is that the Chair can invite and remove members
and SMEs unilaterally. If a Committee has powers designated from the Board,
the default is that all changes of membership must be voted on by the Board
instead.

*Voting.* A Committee’s charter specifies a voting process that it uses for
decision making. The default is the Board's general voting process, applied to
the Committee's smaller set of members. Non-Board SMEs are excluded from
voting unless explicitly approved by the Board.

*Reporting.* A Committee must have a defined reporting cadence and method,
which its Chair is expected to adhere to. The default is monthly verbal
updates during a regular Board meeting or, if that is not possible, by an
email update to the Board. In addition, a tactical committee must provide a
final, official debrief soon after its term has ended.

*Storage.* A Committee must determine a dedicated space for persistent storage
of its documentation and deliverables, subject to the Board's transparency
policy.

*Lifecycle.* As introduced above, a Committee's properties are captured in its
charter. The Board forms a Committee by voting to accept its charter by simple
majority. At any time, the Board can vote to disband a Committee by simple
majority. The Secretary is responsible for updating the State of the Haskell
Foundation document when Committees are created or disbanded.

A Committee’s charter may be amended by the Board at any time. The charter
also includes a section describing whether and how it may be amended by the
Committee itself.

*Charter template.* There is a [template](charter-template.md) we can use to
create Committee charters.

# 9. Voting

This section of our ways of working lays out the details of how our voting processes work.

## 9.1. Preamble

### 9.1.1. Principles

The Haskell Foundation Board seeks to lead the Haskell community forward
through active debate and discussion, where all (both Board members and
members of the Haskell community more broadly) feel included and have a voice.
We expect that these discussions will usually lead to decisions that can be
taken by consensus, and barely need a vote.

It is important, however, to know what decisions are on the table for debate;
to know who is responsible for taking those decisions; and to ensure that
decisions are (after suitable debate) actually taken in a timely way. This
chapter therefore lays out the way in which the Board takes decisions. Its
formality is for the purposes of clarity and precision. We expect that in most
cases the decision will emerge naturally from debate.

1. The Board makes all of its binding decisions through voting. The votes are recorded and, subject to our
   transparency policy, published by the Secretary.
1. Our approach to voting must allow for quick decision making
   while allowing for the necessary scrutiny if required.
1. We encourage debate of all issues subject to voting. This is to make sure that
   all votes represent fully informed opinions and that all members of the board are engaged and feel
   ownership for all decisions of the board.
1. The Board may delegate powers/decisions to a sub-committee, which itself takes formal decisions through voting.

1. Over time, the audit log of past votes will serve as a system of "case law" that will guide us to select
   the proper attributes (see section 9.2.3) of a vote at hand. We bootstrap this with section 9.6.

### 9.1.2. Approach

1. This chapter of our ways of working lays out the definition of a *vote*, with many
   attributes (see section 9.2.3)
   that describe how the vote should play out. However, it does *not* describe
   how these attributes might be amended after a vote is introduced. This is intentional:
   while amending a vote's attributes (including, for example, changing its deadline) can
   be impactful on how the Board makes decisions, finely detailing these rules would lead
   us to many dark corners. (Do we need a vote to conclude debate? Would we need a quorum
   in order to extend a deadline when not enough people have shown up in a meeting?) We
   have decided that it is better to leave these details unspecified and to rely on common
   sense and a spirit of shared enterprise (that is, a non-adversarial Board) to work
   these out as necessary.
1. In practice, details of a vote can be amended by the Chair of the Board/a Committee. These
   amendments should be communicated clearly and in a timely fashion and should follow
   the general will of the Board (or Committee). A Chair who abuses this power should have
   their leadership called into question.
1. In keeping with the Principles outlined above, we expect that voters will sometimes
   need to request more time and/or wish to make sure members absent at a particular
   meeting are included. It is expected that the Chair be sympathetic to such requests,
   while balancing the needs for quick decision-making where time is of the essence.

## 9.2. Definitions

### 9.2.1. Groups

1. A *group* $`G`$ is a set of people, typically the Board or a subset of the Board. An individual may be a
   *member* of a group, and a group has a natural-number *size*, written $`|G|`$. 
1. A *simple majority* of a group $`G`$ is a subset $`S`$ of $`G`$ such that $`|S| > |G| / 2`$. Note that a
   simple majority is a strict greater-than: a group of size 10 has a simple majority of size 6 or greater.
1. A *two-thirds majority* of a group $`G`$ is a subset $`S`$ of $`G`$ such that
   $`|S| \geq |G| * \frac{2}{3}`$. Note that a two-thirds majority is defined using greater-than-or-equals:
   a group of size 9 has a two-thirds majority of size 6 or greater.

### 9.2.2. Sub-groups

This subsection lays out the various subset groups that are relevant for a given vote.

1. The *voting group* of a vote is the group of people who can participate in a vote. A voting group is
   often the entire Board or some Committee of the Board.
1. The *eligible group* of a vote is a subset of the *voting group* that is eligible to vote on a particular
   topic. A member of the voting group might be ineligible because of a conflict of interest or other reason 
   for recusal. Eligibility is not an attribute of the vote (that is, a vote does not define its eligible 
   group), but instead should be considered a property of the individual members of the voting group. We
   expect members to proactively recuse themselves from a vote if appropriate.
   Absent other information, we assume that all members of a voting group are eligible, and thus that the
   eligible group is the same as the voting group.
1. The *participating group* of a vote is a subset of the *eligible group* of that vote that submits a vote.
   A member of the *participating group* may submit the special vote **abstain**, which allows that member
   to be in the *participating group* without casting a vote.
1. The *decision group* of a vote is a subset of the *participating group* that does not submit the special
   vote **abstain**.

### 9.2.3 Vote Attributes

A *vote* is a recorded, archived act of communication in an official channel. A vote has several
*attributes*, defined below: *voting group*, *quorum level*, *choices*, *method*, *transparency
level*, *deadline*, and *tabulator*.

1. The *voting group*, as defined in section 9.2.2.
1. The *quorum level* of a vote is the proportion of the *eligible group* for a vote that must be in the 
   *participating group* in order for the vote to be legitimate. A typical quorum level is *simple majority*.
1. The *choices* of a vote are a set of options that are to be considered by voters. Examples
   include {"yes", "no"} and the set of people nominated for a committee.
1. The *method* of a vote is how the vote communication takes place.
   This attribute is subject to change in case of escalations.
1. The *transparency level* of a vote describes who may access what information about individuals' selections
   during a vote. Typical transparency levels include public (all individuals' votes are known to the wider 
   public), private (individuals' votes are known within the *eligible group* but not beyond it), and anonymous
   (one person receives the votes and tabulates a result, but does not share individuals' selections).
1. The *deadline* of a vote is an event which marks the end of the voting process.
   This attribute is subject to change in case of escalations.
1. The *tabulator* of a vote is a member of the vote's *eligible group* who is responsible for tallying up
   the votes and determining the result of a vote. Tabulation is formulaic and does not involve
   decision-making; the motivation for including a *tabulator* in the attributes of a vote is merely to
   clarify who is responsible for completing this work.

## 9.3. Voting

1. All individuals in a voting group must be given advanced notice of an upcoming vote at least one week
   before the deadline of the vote. This notice happens in an official communication channel, typically
   email or email notification (though a minuted mention in meeting is acceptable).
1. All members of the *eligible group* are invited to submit their vote by the method of the vote.
   The special vote **abstain** is possible.
1. The form of the submission depends on the *choices* of the vote
    1. For votes whose choices are "yes"/"no", the *submission* is one of the two choices.
    1. For votes whose choices are other than "yes"/"no", the *submission* is a ranked ordering of the
       *choices*. The submission may denote some options as equivalent. Any choice not included in a ranked
       ordering is considered less desirable than all included choices. For example, if the choices are A, B, 
       C, and D, then valid votes include:
          * A > B > C > D
          * B > A > D > C
          * A > C = D > B (C and D are equally attractive here)
          * A > B (this is equivalent to A > B > C = D)
          * D (this is equivalent to D > A = B = C)
          * A = B = C = D (this is equivalent to **abstain**)
1. Members may submit multiple votes; only the last submission counts. This allows for members to change their
   mind.
1. After the vote's *deadline* has passed, no more submissions will be accepted.

## 9.4. Vote Results

All votes lead to a *result*, which is a binding decision of the *voting group*.

1. For a vote *result* to be tabulated and recorded, the *participating group* must be at least at the
   *quorum level* of the *eligible group*.
1. After a vote's *deadline* has passed, the *tabulator* is responsible for determining the result:
    1. The tabulator gathers the last vote each member of the *participating group* has submitted.
    1. All **abstain** votes are discarded. (The **abstain** votes are used only distinguish members of the
       *participating group* from the non-participating members of the *eligible group*. This may be 
       important for reaching the *quorum level* of the vote.)
1. The determination of the *result* depends on the choices of the vote
    1. For votes whose choices are "yes"/"no", the vote has a *threshold*. The *threshold* of a vote is
       the *quorum level* of the *decision group*. If the number of "yes" votes meets or exceeds the
       *threshold*, the result of the vote is "yes". If the number of "yes" votes does not meet the
       *threshold* then the result is "no".
    1. For votes whose choices are other than "yes"/"no", the *result* is a ranked ordering of the *choices*
       of the vote. The tabulator runs the [Shulze method](https://en.wikipedia.org/wiki/Schulze_method), for
       example by this [online implementation](https://www.condorcet.vote/), on the votes. The output of the
       Shulze method is the *result* of the vote.
1. The tabulator announces the result to the *voting group* via an official communication, such as email or a
   minuted announcement in a meeting. This result is then the official decision of the *voting group*.

## 9.5 Examples

### 9.5.1. Example "yes"/"no" vote
1. Let's say there is a "yes"/"no" vote (choices)
1. The vote requires a simple majority (quorum level).
1. The board consists of 14 members (voting group).
1. One of them has a conflict of interest (size of eligible group is 13).
1. Out of those, 2 don't show up to the board meeting where the vote is scheduled to take place
   (size of participating group is 11, which is above the quorum level of the eligible group).
1. 3 members abstain from voting (size of decision group is 8, the threshold is the simple majority of 8
   which is 5).
1. 6 members vote for "yes", two vote for "no" (vote result is "yes").

## 9.6. Vote Descriptions

This section describes typical attributes for votes.

1. Operational vote, used to accept the formation of a committee

    1. The *voting group* is the entire Board.
    1. The *quorum level* is *simple majority*.
    1. The *choices* are "yes"/"no".
    1. The *method* is indicated in the vote announcement.
    1. The *transparency level* is public.
    1. The *deadline* is one week.
    1. The *tabulator* is the Secretary of the Board, unless the Secretary is not in the *eligible group*.
       In that case, the Vice Secretary is the *tabulator*. If neither the Secretary nor the Vice Secretary
       is eligible, then the vote organizer must name another *tabulator*.
 
1. Two-thirds vote, used for more contentious/long-lasting decisions, such as functional amendments to the 
   bylaws/ways of working or the ejection of a board member.
   
    1. The *voting group* is the entire Board.
    1. The *quorum level* is *two-thirds majority*.
    1. The *choices* are "yes"/"no".
    1. The *method* is indicated in the vote announcement.
    1. The *transparency level* is public.
    1. The *deadline* is two weeks.
    1. The *tabulator* is chosen as done for the operational vote.

1. *Board selection vote*, used to select new members to join the Board, in accordance with our
   [chapter on the lifecycle of the Board](https://gitlab.haskell.org/hf/meta/-/blob/main/board.md#7-board-membership-lifecycle).
   
    1. The *voting group* is the non-expiring members of the Board.
    1. The *quorum level* is *simple majority*.
    1. The *choices* are the set of applicants plus the special choice **"That's it"**.
    1. The *method* is by off-list email.
    1. The *transparency* level is private.
    1. The *deadline* is April 1st of every year.
    1. The *tabulator* is the individual responsible for running the Board selection process.

   These rules are meant to agree with the rules in the section on the Board's lifecycle; they are
   included here to show how our general scheme of classifying votes scales to this use-case.
