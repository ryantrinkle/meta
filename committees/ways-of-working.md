# Ways of working

## Purpose

Bootstrap the Board by defining its rules of self governance. The includes both the legally binding bylaws and informal rules that we agree to abide to.

## Responsibilities
Drive the board towards defining all of its rules of self governance.

## Delegated powers
None.

This committee is delivering proposals for the board to vote on.

## Term
This committee concludes once all open questions of self governance have been answered and ratified by the Board.

We deliver a sequence of individual "chapters". This will yield tangible progress early, and also allow the other board members to digest and get used to our bylaws and ways of working in manageable chunks. Also, we don't block the important topics with the long tail.

We track the status here in this [project plan](https://docs.google.com/spreadsheets/d/1iG_HrYbaoQGodPioMINhtS9YxrRwNwI8roxuPlIylDI/edit#gid=0).

## Membership

* Chair: Alexander Bernauer
* Other members:
    * Richard Eisenberg
    * Ryan Trinkle

## Membership Rules

The Chair may unilaterally add and remove members, but is expected
to do so in consultation with others and must notify the Board.

If you would like to contribute, please reach out to the Chair.

## Voting Procedure

This committee agrees on draft versions unanimously. These drafts then get voted on by the Board.

## Reporting

The Committee will report to the Board at least monthly, either by a briefing
at a Board meeting or, if that is not possible, by an email update to the
Board.

## Documents

The Committee stores its working documents and deliverables in a subfolder
named Ways of Working in the *Committees* folder in the Haskell
Foundation Google Drive folder ([here](https://drive.google.com/drive/folders/12dyZcbZGEf6ZPcARJY81z-gM-9EkoTOb)).

The final deliverables are merged to [the HF Gitlab repo](https://gitlab.haskell.org/hf/meta/-/tree/main).


## Committee Bylaws

Any change to this charter requires a vote of the Board, with two exceptions:

* The Committee may change its method of document storage and repositories.
* The Committee may make changes to its membership according to the rules set
out in the *Membership Rules* section above.

Any exercise of these exceptions must be accompanied by a notification to the
Board in a Board meeting or by email.
